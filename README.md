# PRL Parallel Computing Framwork Playbook #

This is repository contains Programming Research Laboratory's Ansible
playbook for setting up a parallel, distributed computing framework
stack on a cluster. The stack currently consists of Hadoop, Pregel+,
and Giraph.

The playbook can be used to setup physical, local machines, or remote
virtual machines (such as Amazon EC2 instances).

## Getting Started ##

Modify the `hosts` file and specify the IP address of each node. For
example, the following shows contents of a proper hosts file that
contains information about a local cluster that has four machines, one
master and three slaves.

    [master]
    master ansible_host=10.0.1.11 #private_ip=172.31.28.37

    [slaves]
    slave1 ansible_host=10.0.1.12 #private_ip=172.31.28.38
    slave2 ansible_host=10.0.1.13 #private_ip=172.31.28.39
    slave3 ansible_host=10.0.1.14

However, if the nodes are Amazon EC2 instants, then you must also
uncomment and specify the `private_ip` variable of each node with
their respective EC2 private IP. The following shows contents of a
hosts file that specifies details of five EC2 nodes (one master, four
slaves).

    [master]
    master ansible_host=52.0.2.11 private_ip=172.31.28.35

    [slaves]
    slave1 ansible_host=52.0.2.12 private_ip=172.31.28.36
    slave2 ansible_host=52.0.2.13 private_ip=172.31.28.37
    slave3 ansible_host=52.0.2.14 private_ip=172.31.28.38
    slave4 ansible_host=52.0.2.15 private_ip=172.31.28.39

Also, if the EC2 nodes is using Ubuntu 16.04 image, then you **must**
first run the included fabric file in the `ansible_bootstrap_fabric`
folder by first modifying the `env.hosts` list in the `fabfile.py`
file with the public IPs of all EC2 nodes. Once that is done, run the
file by issuing the following command:

    cd ansible_bootstrap_fabric/
    fab -i /path/to/ec2-cluster-key-file.pem -u $remote_user$ init_cluster

where `$remote_user$` is the name of remote user that has sudo
privilege.

Then, open the `site.yaml` file, and set the variable `remote_user` to be
the name of a user account on remote nodes that has sudo privilege and
will be the one to manage and run all of the frameworks.

Then, run the playbook using the following command:

    ansible-playbook -i hosts site.yaml -K

or if your nodes are on Amazon EC2:

    ansible-playbook -i hosts site.yaml --key-file=/path/to/ec2-cluster-key-file.pem

Once the playbook finishes its run, the next step is to login to the
master node and generate a new password-less key pair, and distribute
the public key to the slave nodes. Then once that is done, format the
HDFS with the following command:

    hdfs namenode -format

then starts the HDFS daemon with the following command:

    start-dfs.sh

and the YARN daemon with this command:

    start-yarn.sh

## Dependencies ##

* SSH
* Python
* Fabric (Optional)
* Ansible version 2.2 or newer

## Author ##

* Smith Dhumbumroong (<zodmaner@gmail.com>)
